Dev Skills and Dev Tools Introduction

What is Documentation?
In the software development process, software documentation is the information that describes the product to the people who develop, deploy and use it.
Documentation comes in many different forms. It may be through online resources, Markdown/README files and manuals and in other cases comments found in the source code itself.

How Important is Documentation?
As developers, we don’t memorize a lot of information especially due to the rapid changes in technology.
Therefore, we rely on different skills and best practices in order to cope up with the speed of progress.
Learning how to properly understand documentation and implement solutions found from different sources is a key skill that every developer should have.
The most efficient developers are those who master the skill of dealing with documentation and know how to work their way around them.

Best Practices for Documentation
There are a lot of best practices and ways for you to become a great developer, the following are some things you can do:
1. Use effective search terms
2. Read official documentation
3. Look for reliable sources of information
4. Scanning and Reading
5. Test your understanding